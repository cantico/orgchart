<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
include "base.php";


function orgchart_onDeleteAddon()
{
    $addon = bab_getAddonInfosInstance('orgchart');
    $addon->removeEventListener('bab_eventBeforeSiteMapCreated', 'orgchart_onBeforeSiteMapCreated', 'events.php');
    $addon->unregisterFunctionality('OrgChart');

    return true;
}


function orgchart_upgrade($version_base, $version_ini)
{
    require_once dirname(__FILE__).'/functions.php';
    
    $addon = bab_getAddonInfosInstance('orgchart');
    $addon->addEventListener('bab_eventBeforeSiteMapCreated', 'orgchart_onBeforeSiteMapCreated', 'events.php');
    $addon->registerFunctionality('OrgChart', 'orgchart.functionalities.class.php');

    orgchart_rePrimary();
    // orgchart_fixOrgCharts();

    return true;
}




function orgchart_rePrimary()
{
    if(!bab_isUserAdministrator()){
        return false;
    }

    global $babDB;

    $sql = "SELECT id
            FROM bab_oc_roles_users

            WHERE id_user NOT IN (
                SELECT id_user FROM bab_oc_roles_users WHERE isprimary = 'Y'
            )

            GROUP BY id_user
            ;";

    $res = $babDB->db_query($sql);

    while($arr = $babDB->db_fetch_array($res)){
        $babDB->db_query("update bab_oc_roles_users set isprimary='Y' where id='".$arr['id']."'");
    }
}


function orgchart_onGroupCreate($id)
{
    require_once $GLOBALS['babInstallPath']."utilit/ocapi.php";
    require_once $GLOBALS['babInstallPath']."utilit/groupsincl.php";

    $ancestors = bab_Groups::getAncestors($id);
    end($ancestors);
    $parentId = key($ancestors);
    if($id > 3 && $parentId > 3){
        $entityName = bab_getGroupName($id, false);
        $entities = bab_OCGetEntitiesByGroupID($parentId);
        foreach($entities as $entity){
            $orgUtil = new bab_OrgChartUtil($entity['id_oc']);
            $children = bab_OCGetDirectChildren($entity['id']);
            $toCreate = true;
            foreach($children as $child){
                if($child['name'] == $entityName){
                    $orgUtil->updateGroupEntity($child['id'], $id);
                    $toCreate = false;
                    break;
                }
            }
            if($toCreate){
                if (!$orgUtil->createEntity($entity['id'], $entityName, $entityName, null, BAB_OC_TREES_LAST_CHILD, $id, $parentId)) {
                    // add trigger_error because groups can be created with ldap_generic
                    $babBody = bab_getBody();
                    if ($babBody->msgerror) {
                        trigger_error(sprintf('Entity creation for group %s: %s', $entityName, $babBody->msgerror));
                    }
                }
            }
        }
    }
}


/*function orgchart_onGroupMove($param)
{
    require_once $GLOBALS['babInstallPath']."utilit/ocapi.php";
    require_once $GLOBALS['babInstallPath']."utilit/groupsincl.php";
    require_once $GLOBALS['babInstallPath'].'utilit/grpincl.php';

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/bab/orgchart/');
    $group = $registry->getValue('group', NULL);
    if($group === null || !bab_isGroup($group)){
        $group = bab_createGroup('OrgChart', 'OrgChart default group', '', BAB_REGISTERED_GROUP);
        $registry->changeDirectory('/bab/orgchart/');
        $registry->setKeyValue('group', $group);
    }

    if(bab_isGroup($param['id'], $group, true) || bab_isGroup($param['oldparent'], $group, true)){
        $ocGroups = bab_getGroups($group, false);
        foreach($ocGroups['id'] as $k => $ocGroupId){
            if(bab_isGroup($param['id'], $ocGroupId) || bab_isGroup($param['oldparent'], $ocGroupId)){
                $ocid = bab_OCGetOrgChartByName($ocGroups['name'][$k]);
                if($ocid){
                    orgchart_Controller()->Orgchart()->reconstructFromGroup($ocid, $ocGroupId);
                }
            }
        }
    }
}*/
