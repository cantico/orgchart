<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
include_once 'base.php';
require_once dirname(__FILE__).'/functions.php';


/**
 *
 */
function orgchart_onBeforeSiteMapCreated(bab_eventBeforeSiteMapCreated $event)
{
    require_once $GLOBALS['babInstallPath'].'utilit/userincl.php';
    
    if (!bab_orgchartAccess()) {
        return;
    }
        
    $addon = bab_getAddonInfosInstance('orgchart');
    bab_functionality::includeOriginal('Icons');


    $position = array('root', 'DGAll', 'babUser', 'babUserSection');

    $item = $event->createItem('orgchart_list');
    $item->setLabel(orgchart_translate('Orgchart'));
    $item->setDescription(orgchart_translate('Orgchart list'));
    $item->setLink($addon->getUrl().'main&idx=orgchart.displaylist');
    $item->addIconClassname(Func_Icons::APPS_ORGCHARTS);
    $item->setPosition($position);

    $event->addFunction($item);
}
