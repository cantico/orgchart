<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

function orgchart_translate($str, $str_plurals = null, $number = null)
{
	if ($translate = bab_functionality::get('Translate/Gettext'))
	{
		/* @var $translate Func_Translate_Gettext */
		$translate->setAddonName('orgchart');
		
		return $translate->translate($str, $str_plurals, $number);
	}
	
	return $str;
}


/**
 * Initialize mysql ORM backend.
 */
function orgchart_loadOrm()
{
	if (!class_exists('ORM_MySqlRecordSet'))
	{
		$Orm = bab_functionality::get('LibOrm');
		/*@var $Orm Func_LibOrm */
		$Orm->initMySql();
		
		$mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
		ORM_MySqlRecordSet::setBackend($mysqlbackend);
	}
}


/**
 * @return orgchart_Controller
 */
function orgchart_Controller()
{
	require_once dirname(__FILE__) . '/controller.class.php';
	return bab_getInstance('orgchart_Controller');
}


/**
 * @return bool
 */
function orgchart_isManager($ocid = null)
{
	if($ocid === null){
		$array = bab_getAccessibleObjects(BAB_OCUPDATE_GROUPS_TBL, bab_getUserId());
		if(empty($array)){
			return false;
		}else{
			return true;
		}
	}else{
		return bab_isAccessValid(BAB_OCUPDATE_GROUPS_TBL, $ocid);
	}
}


/**
 * @return bool
 */
function orgchart_isViewer($ocid = null)
{
	if($ocid === null){
		$array = bab_getAccessibleObjects(BAB_OCVIEW_GROUPS_TBL, bab_getUserId());
		if(empty($array)){
			return false;
		}else{
			return true;
		}
	}else{
		return bab_isAccessValid(BAB_OCVIEW_GROUPS_TBL, $ocid);
	}
}


/**
 * Get root group where all charts are saved
 * @return int
 */
function orgchart_getRootGroup()
{
    require_once $GLOBALS['babInstallPath'].'utilit/grpincl.php';
    
    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/bab/orgchart/');
    $group = $registry->getValue('group');
    if (!$group || $group === null || !bab_isGroup($group)) {

        $group = bab_createGroup('OrgChart', 'OrgChart default group', '', BAB_REGISTERED_GROUP);
        
        if (!$group) {
            
            // creation failed, try to get by name
            
            $arr = bab_getGroups(BAB_REGISTERED_GROUP, false);
            $names = array_flip($arr['name']);
            
            if (isset($names['OrgChart'])) {
                $group = (int) $arr['id'][$names['OrgChart']];
            } else {
            
                $babBody = bab_getBody();
                trigger_error($babBody->msgerror);
                return 0;
            }
        }
        
        $registry->setKeyValue('group', $group);
    }
    
    return $group;
}



/**
 * Get the group associated to org chart name
 * @return int
 */
function orgchart_getChartGroup($ocid)
{
    $group = orgchart_getRootGroup();
    $ocinfo = bab_OCGetOrgChart($ocid);
    
    if ($group <= 3) {
        throw new Exception(sprintf('Wrong orgCharts root group %s', $group));
    }
    
    $ocGroups = bab_getGroups($group, false);
    if (!empty($ocGroups)) {
        foreach ($ocGroups['name'] as $k => $ocGroupName) {
            if ($ocGroupName == $ocinfo['name']) {
                return (int) $ocGroups['id'][$k];
            }
        }
    }
    
    // group not found by name, create orgchart group
    return bab_createGroup($ocinfo['name'], '', 0, $group);
}




/**
 * Dans des versions plus anciennes du noyau
 * les entites ne pouvais pas etre mises a jour lorsque l'organigramme etait verrouille
 * les utilisateurs ajoutes dans les groupes n'ont pas tous ete inseres dans l'entite corespondante
 * cette fonction permet de corriger une entitee lorsque c'est necessaire
 * 
 * @param int $ocid
 * @param int $id_entity
 * 
 * @return mixed    null: not linked to a group; 
 *                  false: entity is up to date;
 *                  true: entity fixed
 */
function orgchart_fixEntity($ocid, $id_entity)
{
    require_once $GLOBALS['babInstallPath'].'utilit/ocapi.php';
    global $babDB;
    
    $orgUtil = new bab_OrgChartUtil($ocid);
    
    $entity = $orgUtil->getEntity($id_entity);
    $id_group = (int) $entity['id_group'];
    
    if (!$id_group) {
        return null;
    }
    
    $entityMembers = array();
    $res = bab_OCselectEntityCollaborators($id_entity, false, true, true);
    while ($member = $babDB->db_fetch_assoc($res)) {
        $entityMembers[$member['id_user']] = true;
    }
    
    // get role for insertions
    
    $roles = $orgUtil->getRoleByType($id_entity, BAB_OC_ROLE_MEMBER);
    $role = reset($roles);
    
    try {
        $groupMembers = bab_getGroupsMembers($id_group, true, true);
    } catch(Exception $e) {
        bab_debug($e->getMessage());
        // linked to a deleted group?
        return null;
    }
    
    $status = false;
    
    foreach ($groupMembers as $user) {
        if (!isset($entityMembers[$user['id']])) {
            // add user to entity
            if ($orgUtil->createRoleUser($role['id'], $user['id'])) {
                $status = true;
            }
        }
    }
    
    return $status;
}


/**
 * Call in upgrade
 * fix orgcharts and output message to console
 */
function orgchart_fixOrgCharts()
{
    require_once $GLOBALS['babInstallPath'].'utilit/ocapi.php';
    
    $charts = bab_OCGetGroupDirOrgCharts();
    foreach ($charts as $ocid => $name) {
        
        $entities = bab_OCGetEntities($ocid);
        foreach ($entities as $entity) {
            
            if (orgchart_fixEntity($ocid, $entity['id'])) {
                bab_installWindow::message(sprintf(orgchart_translate('%s: fix member list in entity %s'), $name, $entity['name']));
            }
        }
    }
}


/**
 * Get parent entity from node ID
 * @param int $ocid     Orgchart ID
 * @param int $idNode
 * @return array
 */
function orgchart_getParentEntity($ocid, $idNode)
{
    global $babDB;
    
    $oBabTree = new bab_dbtree(BAB_OC_TREES_TBL, $ocid);
    $node = $oBabTree->getNodeInfo($idNode);
    $parentNode = $oBabTree->getNodeInfo($node['id_parent']);
    
    $sQuery =
    'SELECT * FROM ' .
    BAB_OC_ENTITIES_TBL . ' ' .
    'WHERE ' .
    'id_node = ' . $babDB->quote($parentNode['id']) . ' AND ' .
    'id_oc = ' . $babDB->quote($ocid);
    
    //bab_debug($sQuery);
    $oResult = $babDB->db_query($sQuery);
    return $babDB->db_fetch_assoc($oResult);
}
