<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../controller.class.php';
require_once dirname(__FILE__).'/../ui/orgchart.ui.php';
require_once $GLOBALS['babInstallPath']."utilit/ocapi.php";
require_once $GLOBALS['babInstallPath']."utilit/dirincl.php";
require_once $GLOBALS['babInstallPath']."utilit/userincl.php";


bab_functionality::includeOriginal('Icons');


/**
 *
 */
class orgchart_CtrlOrgchart extends orgchart_Controller
{
    public function putUserInGroup($ocid = null)
    {
        if(!bab_isUserAdministrator() || $ocid == null){
            die;
        }

        global $babDB;

        $sql = "SELECT dbe.id_user, oe.id_group
                FROM bab_dbdir_entries as dbe

                LEFT JOIN bab_oc_roles_users as oru
                ON oru.id_user = dbe.id

                LEFT JOIN bab_oc_roles as oro
                ON oro.id = oru.id_role

                LEFT JOIN bab_oc_entities as oe
                ON oe.id = oro.id_entity

                WHERE oe.id_oc = ".$babDB->quote($ocid).";";

        $res = $babDB->db_query($sql);

        while($arr = $babDB->db_fetch_array($res)){
            bab_addUserToGroup($arr['id_user'], $arr['id_group']);
        }


        global $babBody;
        $babBody->setTitle('Users added');
    }


    public function displayReconstruct($ocid)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();
        $page->addStyleSheet('addons/orgchart/orgchart.css');

        $controller = orgchart_Controller()->Orgchart();

        $page->addItemMenu('orgchart', orgchart_translate('Orgcharts'), $controller->displayList()->url());
        $page->addItemMenu('reconstruction', orgchart_translate('Group reconstruction'), '');

        $page->setCurrentItemMenu('reconstruction');

        $layout = $W->VBoxItems();

        $layout->addClass('widget-bordered');
        $layout->addClass('widget-centered');
        $layout->addClass('BabLoginMenuBackground');
        $layout->addClass(Func_Icons::ICON_LEFT_16);

        $layout->addItems(
            $W->Label(sprintf(orgchart_translate('The group reconstruction will recreate all groups tree associated to the following orgchart: %s.'), bab_getOrgChartName($ocid))),
            $W->Label(orgchart_translate('All currently associated group to entities will stay where there are in the current group tree but will no longer be linked to entities.')),
            $W->Label(orgchart_translate('As the result when you add member to an entity it will not fill the current group anymore.')),
            $W->Label(orgchart_translate('An important thing to note before proceed all current right associated to current groupe will stay on the current group and will not automatically transfert themself to the new group tree.')),
            $W->Link(
                $W->Icon(
                    orgchart_translate('I fully understand all consequencies of this reconstruction and I want to proceed anyway.'),
                    Func_Icons::ACTIONS_GO_NEXT
                ),
                $controller->reconstruct($ocid)
            )->setConfirmationMessage(orgchart_translate('Proceed to the group reconstruction ?'))
        );

        $page->addItem($layout);

        return $page;
    }



    private function createEntityLevelFromGroup($ocid, $parentEntity, $parentGroup)
    {
        $orgUtil = new bab_OrgChartUtil($ocid);
        $level = bab_getGroups($parentGroup, false);

        if (!$level) {
            return;
        }

        foreach ($level['id'] as $k => $subGroup) {
            $subEntity = $orgUtil->createEntity($parentEntity, $level['name'][$k], $level['description'][$k], null, BAB_OC_TREES_LAST_CHILD, $subGroup, $parentGroup);
            if ($subEntity) {
                $this->createEntityLevelFromGroup($ocid, $subEntity, $subGroup);
            }
        }
    }



    /**
     * Create org chart from group
     * Start from empty org chart and a tree of groups in the orgchart group
     * @param int $ocid
     */
    public function reconstructFromGroup($ocid)
    {
        $orgUtil = new bab_OrgChartUtil($ocid);

        if (!$orgUtil->bHaveUpdateRight) {
            throw new bab_AccessException(orgchart_translate('Acces denied'));
        }

        if (!bab_isUserAdministrator()) {
            throw new bab_AccessException(orgchart_translate('Acces denied, this script is for admin only'));
        }

        $lock = $orgUtil->getLockInfo();

        if ($lock['iIdUser'] && $lock['iIdUser'] != bab_getUserId()) {
            throw new bab_AccessException(orgchart_translate('Locked by another user'));
        }

        if(!$orgUtil->lock()) {
            throw new bab_AccessException(orgchart_translate('Failed to lock'));
        }

        $root = $orgUtil->getRoot();
        if($root) {
            throw new bab_AccessException(orgchart_translate('Build from group can be done only on a empty orgchart'));
        }

        $idChartGroup = orgchart_getChartGroup($ocid);

        $levelOne = bab_getGroups($idChartGroup, false);

        if (!$levelOne) {
            throw new bab_AccessException(sprintf(orgchart_translate('No group found under %s'), $idChartGroup));
        }


        if (1 !== count($levelOne['id'])) {
            throw new bab_AccessException(sprintf(orgchart_translate('Group level one must contain only one group for the root entity, found %s'), implode(', ', $levelOne['name'])));
        }

        $firstEntityGroup = $levelOne['id'][0];

        // create the root entity
        $entity = $orgUtil->createEntity(0, $levelOne['name'][0], $levelOne['description'][0], null, BAB_OC_TREES_LAST_CHILD, $firstEntityGroup, $idChartGroup);

        $this->createEntityLevelFromGroup($ocid, $entity, $firstEntityGroup);
    }


    protected function addRolesToGroup($orgUtil, $idEntity, $idGroup)
    {
        global $babDB;
        $roles = $orgUtil->getRoleByEntityId($idEntity);
        foreach ($roles as $role) {
            $users = bab_OCGetUsersByRole($role['id']);
            while($user = $babDB->db_fetch_assoc($users)) {
                bab_addUserToGroup($user['id_user'], $idGroup);
            }
        }
    }



    private function reconstructEnityChilren($ocid, $entity, $idParentGroup)
    {
        $orgUtil = new bab_OrgChartUtil($ocid);
        $entities = bab_OCGetDirectChildren($entity);
        foreach($entities as $entity){
            $idGroup = bab_createGroup($entity['name'], 'entity', '', $idParentGroup);
            $orgUtil->updateGroupEntity($entity['id'], $idGroup);
            $this->addRolesToGroup($orgUtil, $entity['id'], $idGroup);
            $this->reconstructEnityChilren($ocid, $entity['id'], $idGroup);
        }
    }


    public function reconstruct($ocid)
    {
        $orgUtil = new bab_OrgChartUtil($ocid);
        $lock = $orgUtil->getLockInfo();
        if($orgUtil->bHaveUpdateRight && (!$lock['iIdUser']  || $lock['iIdUser'] == bab_getUserId()) && bab_isUserAdministrator()){
            if(!$orgUtil->lock()){
                die;
            }
            $root = $orgUtil->getRoot();
            $rootInfo = $orgUtil->getEntity($root['id_entity']);
            //var_dump($rootInfo);
            //die();

            require_once $GLOBALS['babInstallPath'].'utilit/grpincl.php';


            $iIdParentGroup = orgchart_getChartGroup($ocid);

            $idRootGroup = bab_createGroup($rootInfo['name'], 'root group', '', $iIdParentGroup);
            $orgUtil->updateGroupEntity($rootInfo['id'], $idRootGroup);
            $this->addRolesToGroup($orgUtil, $rootInfo['id'], $idRootGroup);
            $this->reconstructEnityChilren($ocid, $rootInfo['id'], $idRootGroup);

            $orgUtil->unlock();

            $GLOBALS['babBody']->addNextPageMessage(orgchart_translate('Groupe reconstruction done.'));
            orgchart_Controller()->Orgchart()->displayList()->location();
        }

    }


    public function displayList()
    {
        $W = bab_Widgets();
        $page = $W->BabPage();
        $page->addStyleSheet('addons/orgchart/orgchart.css');

        $page->addItemMenu('orgchart', orgchart_translate('Orgcharts'), '');

        $page->setCurrentItemMenu('orgchart');

        $orgs = bab_OCGetAccessibleOrgCharts(3);

        $header = array(
            'name' => orgchart_translate('name'),
            'description' => orgchart_translate('description'),
            '_actions' => ''
        );

        $content = array();
        foreach($orgs as $org)
        {
            $content[$org['id']] = array(
                'name' => $org['name'],
                'description' => $org['description'],
                '_actions' => $org['id']
            );
        }

        $table = new orgchart_TableArrayView($header, $content, 'orgchart');
        $table->addClass(Func_Icons::ICON_LEFT_16);

        $page->addItem($table);

        return $page;
    }

    public function unlock($ocid)
    {
        $force = false;
        if(bab_isUserAdministrator()){
            $force = true;
        }

        $orgUtil = new bab_OrgChartUtil($ocid);
        $orgUtil->unlock($force);

        return true;
    }


    public function EditApprobators($ocid, $entity)
    {
        global $babDB;
        $W = bab_Widgets();
        $page = $W->BabPage();

        $orgUtil = new bab_OrgChartUtil($ocid);
        $entityInfo = bab_OCGetEntity($entity);

        $roleInfo = $orgUtil->getRoleByType($entity, BAB_OC_ROLE_SUPERIOR);
        $roleInfo = $roleInfo[0];
        $userRes = bab_OCGetUsersByRole($roleInfo['id']);
        $user = $babDB->db_fetch_assoc($userRes);

        if($user && isset($user['sn'])){
            $actionsApprobators = $W->HBoxItems(
                $W->Label($user['sn'] . ' ' . $user['givenname'])->addClass('widget-strong'),
                $W->HBoxItems(
                    $W->Link(
                        '',
                        orgchart_Controller()->Orgchart()->editMembers($ocid, $entity, $roleInfo['id'])
                    )->setTitle(orgchart_translate('Change the main approbator'))
                    ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    ->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT),
                    $W->Link(
                        '',
                        orgchart_Controller()->Orgchart()->removeMember($user['id'], $ocid)
                    )->addCLass('icon',Func_Icons::ACTIONS_EDIT_DELETE)
                    ->setConfirmationMessage(orgchart_translate('This will remove the associated approbator, proceed?'))
                    ->setAjaxAction(orgchart_Controller()->Orgchart()->removeMember($user['id'], $ocid),'approb-editor-'.$entity)
                )->setVerticalAlign('bottom')
            )->setVerticalAlign('bottom')->setHorizontalSpacing(1,'em');
        }else{
            $actionsApprobators = $W->Link(
                orgchart_translate('Define the main approbator'),
                orgchart_Controller()->Orgchart()->editMembers($ocid, $entity, $roleInfo['id'])
            )->addClass('widget-strong')->setTitle(orgchart_translate('Define the main approbator'))
            ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
            ->addClass('icon', Func_Icons::ACTIONS_USER_NEW);
        }


        $roleInfo = $orgUtil->getRoleByType($entity, BAB_OC_ROLE_TEMPORARY_EMPLOYEE);
        $roleInfo = $roleInfo[0];
        $userRes = bab_OCGetUsersByRole($roleInfo['id']);
        $user = $babDB->db_fetch_assoc($userRes);

        if($user && isset($user['sn'])){
            $actionsApprobatorSupleant = $W->HBoxItems(
                $W->Label($user['sn'] . ' ' . $user['givenname'])->addClass('widget-strong'),
                $W->HBoxItems(
                    $W->Link(
                        '',
                        orgchart_Controller()->Orgchart()->editMembers($ocid, $entity, $roleInfo['id'])
                    )->setTitle(orgchart_translate('Change the main approbator'))
                    ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    ->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT),
                    $W->Link(
                        '',
                        orgchart_Controller()->Orgchart()->removeMember($user['id'], $ocid)
                    )->addCLass('icon',Func_Icons::ACTIONS_EDIT_DELETE)
                    ->setConfirmationMessage(orgchart_translate('This will remove the associated supleant approbator, proceed?'))
                    ->setAjaxAction(orgchart_Controller()->Orgchart()->removeMember($user['id'], $ocid),'approb-editor-'.$entity)
                )->setVerticalAlign('bottom')
            )->setVerticalAlign('bottom')->setHorizontalSpacing(1,'em');
        }else{
            $actionsApprobatorSupleant = $W->Link(
                orgchart_translate('Define the supleant approbator'),
                orgchart_Controller()->Orgchart()->editMembers($ocid, $entity, $roleInfo['id'])
            )->addClass('widget-strong')->setTitle(orgchart_translate('Define the main approbator'))
            ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
            ->addClass('icon', Func_Icons::ACTIONS_USER_NEW);
        }

        $layout = $W->VBoxItems(
            $W->Title(sprintf(orgchart_translate('Change approbators of entity %s'), $entityInfo['name']),3),
            $W->FlowItems(
                $W->Label(orgchart_translate('Main approbator (Superior)'). ': '),
                $actionsApprobators
            )->setVerticalAlign('bottom')->addClass(Func_Icons::ICON_LEFT_24)->setHorizontalSpacing(1, 'em'),
            $W->FlowItems(
                $W->Label(orgchart_translate('Supleant approbator'). ': '),
                $actionsApprobatorSupleant
            )->setVerticalAlign('bottom')->addClass(Func_Icons::ICON_LEFT_24)->setHorizontalSpacing(1, 'em')
        )->setVerticalSpacing(1,'em')->setId('approb-editor-'.$entity)
        ->setReloadAction(orgchart_Controller()->Orgchart()->EditApprobators($ocid, $entity));

        if(bab_isAjaxRequest()) {
            return $layout;
        }

        return $page->addItem($layout);
    }


    public function editEntity($ocid, $currentEntity = null, $parentEntity = null, $view = 'orgchart')
    {
        $W = bab_Widgets();
        $page = $W->BabPage();

        $_SESSION['orgchart_view'] = $view;
        $editor = new orgchart_EntityEditor($ocid, $currentEntity, $parentEntity);

        if(bab_isAjaxRequest()) {
            return $editor;
        }

        return $page->addItem($editor);
    }


    public function confirmCreateGroup($ocid, $entityId)
    {
        if (!orgchart_isManager($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $W = bab_Widgets();
        $page = $W->BabPage();

        $editor = new orgchart_CreateGroupEditor($ocid, $entityId);

        if(bab_isAjaxRequest()) {
            return $editor;
        }

        return $page->addItem($editor);
    }


    /**
     * Create group on entity for entity without group
     */
    public function createGroup($entity = null)
    {
        $this->requireSaveMethod();
        $orgUtil = new bab_OrgChartUtil($entity['orgchart']);

        $lock = $orgUtil->getLockInfo();
        if($orgUtil->bHaveUpdateRight && (!$lock['iIdUser']  || $lock['iIdUser'] == bab_getUserId())){
            if(!$orgUtil->lock()){
                throw new bab_AccessException(orgchart_translate('Access denied'));
            }

            $entity = $orgUtil->getEntity($entity['id']);

            if ($entity['id_group']) {
                throw new bab_SaveErrorException(orgchart_translate('Entity is allready linked to a group'));
            }


            $parent = orgchart_getParentEntity($orgUtil->getOrgChartId(), $entity['id_node']);

            if (!$parent['id_group']) {
                throw new bab_SaveErrorException(orgchart_translate('There is no group linked to the parent entity'));
            }


            $id_group = bab_createGroup($entity['name'], $entity['description'], 0, $parent['id_group']);
            $orgUtil->updateGroupEntity($entity['id'], $id_group);
            $this->addRolesToGroup($orgUtil, $entity['id'], $id_group);
            return true;
        }

        throw new bab_AccessException(orgchart_translate('Access denied'));
    }

    public function saveEntity($entity = null)
    {
        $this->requireSaveMethod();
        $orgUtil = new bab_OrgChartUtil($entity['orgchart']);

        $lock = $orgUtil->getLockInfo();
        if($orgUtil->bHaveUpdateRight && (!$lock['iIdUser']  || $lock['iIdUser'] == bab_getUserId())){
            if(!$orgUtil->lock()){
                throw new bab_AccessException(orgchart_translate('Access denied'));
            }


            $entity['name'] = bab_getStringAccordingToDataBase($entity['name'], 'utf-8');
            $entity['description'] = bab_getStringAccordingToDataBase($entity['description'], 'utf-8');
            if(isset($entity['id']) && $entity['id']){
                $orgUtil->updateEntity($entity['id'], $entity['name'], $entity['description']);
            }else{

                $mixedGroup = 'none';
                $iIdParentGroup = BAB_REGISTERED_GROUP;
                if($entity['parent']){
                    $parentUtil = $orgUtil->getEntity($entity['parent']);
                    if($parentUtil['id_group']>3){
                        $mixedGroup = 'new';
                        $iIdParentGroup = $parentUtil['id_group'];
                    }
                }else{
                    $iIdParentGroup = BAB_REGISTERED_GROUP;
                    $mixedGroup = 'new';
                    $entity['parent'] = 0;
                }

                if($iIdParentGroup == BAB_REGISTERED_GROUP){
                    $iIdParentGroup = orgchart_getChartGroup($entity['orgchart']);
                }

                if (!$orgUtil->createEntity($entity['parent'], $entity['name'], $entity['description'], '', BAB_OC_TREES_LAST_CHILD, $mixedGroup, $iIdParentGroup)) {
                    $babBody = bab_getBody();
                    throw new bab_SaveErrorException($babBody->msgerror);
                }
            }
        }
        return true;
    }


    public function deleteConfirmEntity($ocid, $entityId, $view = 'orgchart')
    {
        if (!orgchart_isManager($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $W = bab_Widgets();
        $page = $W->BabPage();

        $editor = new orgchart_EntityRemover($ocid, $entityId);

        if(bab_isAjaxRequest()) {
            return $editor;
        }

        return $page->addItem($editor);
    }

    public function deleteEntity($entity = null)
    {
        $this->requireDeleteMethod();

        if (!orgchart_isManager($entity['orgchart'])) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $orgUtil = new bab_OrgChartUtil($entity['orgchart']);

        if(!$orgUtil->lock()){
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        if (isset($entity['orgchart']) && $entity['orgchart'] && isset($entity['id']) && $entity['id']) {
            $entities = bab_OCGetChildsEntities($entity['id'], $entity['orgchart']);

            require_once $GLOBALS['babInstallPath'].'utilit/delincl.php';
            foreach($entities as $ent){
                $entityUtil = $orgUtil->getEntity($ent['id']);
                if($entityUtil['id_group'] > 3){
                    bab_deleteGroup($entityUtil['id_group']);
                }
                $orgUtil->deleteEntity($ent['id'], BAB_OC_DELETE_ENTITY_ONLY);
            }

            $entityUtil = $orgUtil->getEntity($entity['id']);
            if($entityUtil['id_group'] > 3){
                bab_deleteGroup($entityUtil['id_group']);
            }
            $orgUtil->deleteEntity($entity['id'], BAB_OC_DELETE_ENTITY_ONLY);

            return true;
        }

        return false;
    }

    public function ManageMembers($ocid, $entityId, $view = 'orgchart')
    {
        if (!orgchart_isManager($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $W = bab_Widgets();
        $page = $W->BabPage();

        $layout = $W->HBoxItems()->setHorizontalSpacing(1,'em')->addClass('widget-table-100pc');

        $layout->addItem(orgchart_Controller()->Orgchart(false)->dispalyRoleList($ocid, $entityId));
        $layout->addItem(orgchart_Controller()->Orgchart(false)->displayUserRoleList($ocid, $entityId));

        if(bab_isAjaxRequest()) {
            return $layout;
        }

        $page->addItem($layout);
        return $page;
    }

    public function dispalyRoleList($ocid, $entityId)
    {
        if (!orgchart_isManager($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $W = bab_Widgets();
        $roleTree = $W->SimpleTreeView('orgchart-simpletree-role')->addClass(Func_Icons::ICON_LEFT_16);
        $roleTree->setReloadAction(orgchart_Controller()->Orgchart()->dispalyRoleList($ocid, $entityId));
        //$roleTree->hideToolbar();
        $roleTree->setPersistent(true, Widget_Widget::STORAGE_COOKIES);

        $orgUtil = new bab_OrgChartUtil($ocid);
        $roles = $orgUtil->getRoleByEntityId($entityId);


        $root =& $roleTree->createElement(
            'roles',
            'icon '.Func_Icons::OBJECTS_PUBLICATION_CATEGORY,
            bab_toHtml(orgchart_translate('Roles')),
            '',
            ''
        );
        //$root->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/category.png');
        $root->addAction(
            'add_role',
            orgchart_translate("Order roles"),
            $GLOBALS['babSkinPath'] . 'images/Puces/a-z.gif',
            orgchart_Controller()->Orgchart()->editRoleOrder($ocid, $entityId)->url(),
            '',
            array('this'),
            'widget-link widget-popup-dialog-and-reload'
        );
        $root->addAction(
            'add_role',
            orgchart_translate("Add a role"),
            $GLOBALS['babSkinPath'] . 'images/Puces/edit_add.png',
            orgchart_Controller()->Orgchart()->editRole($ocid, $entityId)->url(),
            '',
            array('this'),
            'widget-link widget-popup-dialog-and-reload'
        );
        $roleTree->appendElement(
            $root,
            null
        );

        foreach($roles as $role){
            if($role['type'] != 2){
                if(!isset($_SESSION['selected-role-'.$entityId])){
                    $_SESSION['selected-role-'.$entityId] = $role['id'];
                    $roleTree->highlightElement('role' . $role['id']);
                }else{
                    if($_SESSION['selected-role-'.$entityId] == $role['id']){
                        $roleTree->highlightElement('role' . $role['id']);
                    }
                }
                $element =& $roleTree->createElement(
                    'role' . $role['id'],
                    'typeDuh',
                    bab_toHtml($role['name']),
                    '',
                    ''
                );

                if($role['type'] == 1){
                    $icon = Func_Icons::OBJECTS_MANAGER;
                }elseif($role['type'] == 3){
                    $icon = Func_Icons::OBJECTS_GROUP;
                }else{
                    $icon = Func_Icons::OBJECTS_GROUP;
                }


                if($_SESSION['selected-role-'.$entityId] == $role['id']){
                    $element->setItem(
                        $W->Label($role['name'])->addClass('icon', $icon)
                    );
                }else{
                    $element->setItem(
                        $W->Link($role['name'], '#')->addClass('icon', $icon)
                            ->setAjaxAction(orgchart_Controller()->Orgchart()->ajaxRoleSelect($role['id'], $entityId), array('orgchart-simpletree-role','orgchart-userrole-list'))
                    );
                }
                if($role['type'] == 0){
                    $element->addAction(
                        'remove_role',
                        orgchart_translate("Remove this role"),
                        $GLOBALS['babSkinPath'] . 'images/Puces/del.gif',
                        bab_toHtml(orgchart_Controller()->Orgchart()->confirmeRemoveRole($ocid, $role['id'])->url()),
                        '',
                        array('this'),
                        'widget-link widget-popup-dialog-and-reload'
                    );
                }

                $element->addAction(
                    'edit_role',
                    orgchart_translate("Edit this role"),
                    $GLOBALS['babSkinPath'] . 'images/Puces/edit.png',
                    bab_toHtml(orgchart_Controller()->Orgchart()->editRole($ocid, $entityId, $role['id'])->url()),
                    '',
                    array('this'),
                    'widget-link widget-popup-dialog-and-reload'
                );

                $roleTree->appendElement(
                    $element,
                    $root->_id
                );
            }
        }

        //$roleTree->_updateTree();
        return $roleTree->setSizePolicy("widget-col-33pc");
    }

    public function ajaxRoleSelect($role, $entity)
    {
        $_SESSION['selected-role-'.$entity] = $role;
        die();
    }

    public function displayUserRoleList($ocid, $entity)
    {
        if (!orgchart_isManager($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }


        global $babDB;
        $W = bab_Widgets();
        if(isset($_SESSION['selected-role-'.$entity])){
            $users = bab_OCGetUsersByRole($_SESSION['selected-role-'.$entity], true, true);

            $header = array(
                'sn' 		=>	orgchart_translate('Lastname'),
                'givenname' =>	orgchart_translate('Firstname'),
                '_rm' =>	orgchart_translate('Remove')
            );

            $content = array();
            while($user = $babDB->db_fetch_assoc($users)){
                $content[$user['id']] = array(
                    'sn' 		=> $user['sn'],
                    'givenname' => $user['givenname'],
                    '_rm'		=> $user['id']
                );
            }

            $orgUtil = new bab_OrgChartUtil($ocid);
            $roleInfo = $orgUtil->getRoleById($_SESSION['selected-role-'.$entity]);

            $table = new orgchart_userRoleArrayView($header, $content, '', null, 'orgchart-userrole-list-'.$_SESSION['selected-role-'.$entity], $ocid, $entity);
            $table->setPageLength(10);
            $table->setAjaxAction();

            $layout = $W->VBoxItems(
                $W->FlowItems(
                    $W->FlowItems(
                        $W->Link(
                            orgchart_translate('Edit  members of role:'),
                            orgchart_Controller()->Orgchart()->editMembers($ocid, $entity)
                        )
                        ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD),
                        $W->Link(
                            $roleInfo['name'],
                            orgchart_Controller()->Orgchart()->editMembers($ocid, $entity)
                        )->addClass('widget-strong')
                        ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    )->setHorizontalSpacing(.25, 'em')->addClass('widget-action-button', 'icon', Func_Icons::ACTIONS_DOCUMENT_EDIT, 'orgchart-edit-member')
                )->addClass('widget-100pc')->addClass('widget-toolbar'),
                $W->VboxLayout('orgchart-userrole-list')->addItem($table)
            )->addClass(Func_Icons::ICON_LEFT_24)->setVerticalSpacing(1, 'em');

            return $layout->setSizePolicy("widget-col-67pc")->setReloadAction(orgchart_Controller()->Orgchart()->displayUserRoleList($ocid, $entity));
        }
        return $W->Label('');
    }

    public function editMembersTableView($ocid, $entity)
    {
        if (!orgchart_isManager($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $W = bab_Widgets();
        global $babDB;
        if(isset($_SESSION['selected-role-'.$entity])){
            $header = array('_cb' => '');

            $infoOC = bab_OCGetOrgChart($ocid);

            $arr = $babDB->db_fetch_assoc(
                $babDB->db_query(
                    "select id_group
                    from bab_db_directories
                    where id='".$babDB->db_escape_string($infoOC['id_directory'])."'"
                )
            );

            if(bab_isAccessValid(BAB_DBDIRVIEW_GROUPS_TBL, $infoOC['id_directory'])) {
                $idgroup = $arr['id_group'];
                $rescol = $babDB->db_query(
                    "select id, id_field
                    from ".BAB_DBDIR_FIELDSEXTRA_TBL."
                    where id_directory='".($idgroup != 0? 0: $babDB->db_escape_string($infoOC['id_directory']))."'
                    and ordering!='0'
                    order by ordering asc"
                );
                $countcol = $babDB->db_num_rows($rescol);
            } else {
                $countcol = 0;
            }
            $sqlf = array();
            $leftjoin = array();
            $select = array();
            $tmp = array();
            $i = 0;
            $xf = "";
            while($i < $countcol){
                $arr = $babDB->db_fetch_assoc($rescol);
                if( $arr['id_field'] < BAB_DBDIR_MAX_COMMON_FIELDS ) {
                    $rr = $babDB->db_fetch_assoc(
                        $babDB->db_query(
                            "select name, description
                            from ".BAB_DBDIR_FIELDS_TBL."
                            where id='".$babDB->db_escape_string($arr['id_field'])."'"
                        )
                    );
                    $coltxt = bab_toHtml(translateDirectoryField($rr['description']));
                    $header[$rr['name']] = $coltxt;
                    $tmp[] = $rr['name'];
                    $select[] = 'e.'.$rr['name'];
                } else {
                    $rr = $babDB->db_fetch_assoc(
                        $babDB->db_query(
                            "select *
                            from ".BAB_DBDIR_FIELDS_DIRECTORY_TBL."
                            where id='".$babDB->db_escape_string(($arr['id_field'] - BAB_DBDIR_MAX_COMMON_FIELDS))."'"
                        )
                    );
                    $coltxt = bab_toHtml(translateDirectoryField($rr['name']));
                    $filedname = "babdirf".$arr['id'];
                    $header[$filedname] = $coltxt;
                    $sqlf[] = $filedname;

                    $leftjoin[] =
                        'LEFT JOIN '.BAB_DBDIR_ENTRIES_EXTRA_TBL.' lj'.$arr['id']."
                        ON lj".$arr['id'].".id_fieldx='".$arr['id']."'
                        AND e.id=lj".$arr['id'].".id_entry";
                    $select[] = "lj".$arr['id'].'.field_value '."babdirf".$arr['id']."";
                }

                $i++;
            }
            if (count($tmp) > 0 || count($sqlf) > 0) {
                $tmp[] = "id";
                if ($xf == "") {
                    $xf = $tmp[0];
                }

                if ($idgroup > 1) {
                    $req = " " . BAB_DBDIR_ENTRIES_TBL . " e " . implode(' ' , $leftjoin) . "
                         LEFT JOIN " . BAB_USERS_GROUPS_TBL . " ug ON e.id_user = ug.id_object
                         LEFT JOIN " . BAB_USERS_TBL . " AS babusers ON e.id_user = babusers.id
                         WHERE ug.id_group=" . $babDB->quote($idgroup) . "
                         AND e.id_directory='0'";
                } else {
                    $req = " " . BAB_DBDIR_ENTRIES_TBL . " e " . implode(' ', $leftjoin) . "
                        LEFT JOIN " . BAB_USERS_TBL . " AS babusers ON e.id_user = babusers.id
                        WHERE e.id_directory=" . $babDB->quote(1 == $idgroup ? 0 : $infoOC['id_directory']);
                }

                $select[] = 'e.id';
                if (! in_array('email', $select)) {
                    $this->select[] = 'e.email';
                }

                $req .= ' AND ' . bab_userInfos::queryAllowedUsers('babusers');

                $like = '';
                if (isset($_SESSION['MembersFilter' . $entity]) && ! empty($_SESSION['MembersFilter' . $entity])) {
                    $like = ' AND (';
                    foreach ($select as $selectOption) {
                        if ($like != ' AND (') {
                            $like .= ' OR ';
                        }
                        $selectOption = explode(' ', $selectOption);
                        $selectOption = array_shift($selectOption);
                        $like .= $selectOption . " LIKE '%" . $babDB->db_escape_string($_SESSION['MembersFilter' . $entity]) . "%'";
                    }
                    $like .= ')';
                }

                $req = "select " . implode(',', $select) . " from " . $req . " " . $like . " order by `" . $babDB->db_escape_string($xf) . "` ";
                $req .= "asc";

                $res = $babDB->db_query($req);
            }

            $roleInfo = bab_OCGetRole($_SESSION['selected-role-'.$entity]);
            $cardinality = true;
            if($roleInfo['cardinality'] == 'N'){
                $cardinality = false;
            }

            $content = array();
            while($arr = $babDB->db_fetch_assoc($res)){
                $dirEntryArray = array();
                if($cardinality){
                    $dirEntryArray['_cb'] = $arr['id'];
                }else{
                    $dirEntryArray['_rb'] = $arr['id'];
                }
                unset($arr['id']);
                foreach($arr as $k => $v){
                    $dirEntryArray[$k] = $v;
                }
                $content[] = $dirEntryArray;
            }

            $table = new orgchart_userRoleArrayView($header, $content, '', null, 'orgchart-userrole-list-edit-'.$entity, $ocid, $entity);
            $table->setReloadAction(orgchart_Controller()->Orgchart()->editMembersTableView($ocid, $entity));
            $table->setAjaxAction();
            $table->setPageLength(10);
            if(isset($_SESSION['ResetPage'.$entity]) && $_SESSION['ResetPage'.$entity]){
                $_SESSION['ResetPage'.$entity] = false;
                $table->setCurrentPage(0);
            }

            $table->addClass(Func_Icons::ICON_LEFT_24);

            return $table;
        }

        return $W->Label('');
    }

    public function saveMembersForm($filterform = null)
    {
        $entity = $filterform['entity'];
        $_SESSION['MembersFilter'.$entity] = $filterform['text'];
        $_SESSION['ResetPage'.$entity] = true;
        die;
    }

    public function editMembers($ocid, $entity, $role = null)
    {
        if (!orgchart_isManager($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $W = bab_Widgets();

        if($role !== null){
            $_SESSION['selected-role-'.$entity] = $role;
        }

        $filterForm = orgchart_userRoleFilter($entity);

        $table = $this->editMembersTableView($ocid, $entity);

        $vbox = $W->VBoxLayout('filtered-form')
            ->addItem($filterForm)
            ->addItem($table)
            ->setVerticalSpacing(1, 'em')
            ->setReloadAction(orgchart_Controller()->Orgchart()->editMembers($ocid, $entity));

        if(bab_isAjaxRequest()) {
            return $vbox;
        }else{
            return $W->BabPage()->addItem($vbox);
        }
    }

    public function reloadDirectoryItem($direntry, $ocid, $idrole)
    {
        if (!orgchart_isManager($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        global $babDB;
        $W = bab_Widgets();
        static $userArr = null;

        if($userArr === null){
            $usersRes = bab_OCGetUsersByRole($idrole);
            while($userArr[] = $babDB->db_fetch_assoc($usersRes)){}
        }

        $checked = false;
        foreach($userArr as $user){
            if($user['id_dir'] == $direntry){
                $checked = true;
                break;
            }
        }

        return $W->Form('orgcb-'.$direntry)->addItem(
            $W->FlowItems(
                $W->CheckBox()->setValue($checked)
                    ->setName('direntryid')
                    ->setCheckedValue("1")
                    ->setAjaxAction(
                        orgchart_Controller()->Orgchart()->toggleDirectoryItem($direntry, $ocid, $idrole),
                        'orgcb-'.$direntry
                    )
            )
        )->setReloadAction(orgchart_Controller()->Orgchart()->reloadDirectoryItem($direntry, $ocid, $idrole));

    }



    public function toggleDirectoryItem($direntry, $ocid, $idrole)
    {
        if (!orgchart_isManager($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        global $babDB;
        static $userArr = null;

        $checked = false;
        $usersRes = bab_OCGetUsersByRole($idrole);
        while($userArr = $babDB->db_fetch_assoc($usersRes)){

            if($userArr['id_dir'] == $direntry){
                $checked = $userArr['id'];
                break;
            }
        }

        if($checked){
            $this->removeMember($checked, $ocid);
        }else{
            $this->addMember($direntry, $ocid, $idrole);
        }

        die;
    }



    public function setDirectoryItem($direntry, $ocid, $idrole)
    {
        if (!orgchart_isManager($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $orgUtil = new bab_OrgChartUtil($ocid);
        $orgUtil->deleteRoleUserByRoleId($idrole);
        $orgUtil->createRoleUser($idrole, $direntry);

        die;
    }

    public function addMember($direntry, $ocid, $idrole)
    {
        if (!orgchart_isManager($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $orgUtil = new bab_OrgChartUtil($ocid);
        $orgUtil->createRoleUser($idrole, $direntry);
        die;
    }

    public function removeMember($member, $ocid)
    {
        if (!orgchart_isManager($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $orgUtil = new bab_OrgChartUtil($ocid);
        $orgUtil->deleteRoleUserByRoleUserId($member);
        die;
    }

    public function editRole($ocid, $entityId, $roleId = null)
    {
        if (!orgchart_isManager($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $W = bab_Widgets();
        $page = $W->BabPage();

        $editor = new orgchart_RoleEditor($ocid, $entityId, $roleId);

        if(bab_isAjaxRequest()) {
            return $editor;
        }

        return $page->addItem($editor);
    }


    public function saveRole($role = null)
    {
        if (!orgchart_isManager($role['ocid'])) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $orgUtil = new bab_OrgChartUtil($role['ocid']);
        $role['name'] = bab_getStringAccordingToDataBase($role['name'], 'utf-8');
        $role['description'] = bab_getStringAccordingToDataBase($role['description'], 'utf-8');
        if($role['id']){
            $orgUtil->updateRole($role['id'], $role['name'], $role['description']);
        }else{
            $orgUtil->createRole($role['entity'], $role['name'], $role['description'], BAB_OC_ROLE_CUSTOM, 'Y');
        }

        die;
    }

    public function confirmeRemoveRole($ocid, $role)
    {
        if (!orgchart_isManager($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $W = bab_Widgets();
        $page = $W->BabPage();

        $editor = new orgchart_RoleRemover($ocid, $role);

        if(bab_isAjaxRequest()) {
            return $editor;
        }

        return $page->addItem($editor);

        die;
    }


    public function removeRole($role = null)
    {
        if (!orgchart_isManager($role['ocid'])) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $orgUtil = new bab_OrgChartUtil($role['ocid']);
        $orgUtil->deleteRole($role['id']);

        die;
    }


    public function editRoleOrder($ocid, $entityId)
    {
        if (!orgchart_isManager($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $W = bab_Widgets();
        $page = $W->BabPage();

        $editor = new orgchart_RoleOrderEditor($ocid, $entityId);

        if(bab_isAjaxRequest()) {
            return $editor;
        }

        return $page->addItem($editor);
    }


    public function saveRoleOrder($role = null)
    {
        if (!orgchart_isManager($role['ocid'])) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $orgUtil = new bab_OrgChartUtil($role['ocid']);
        $iOrder = 1;
        $keys = array_keys($role['order']);
        foreach ($keys as $k) {
            $orgUtil->updateRoleOrder($k, $iOrder);
            $iOrder++;
        }

        die;
    }


    public function Move($ocid, $entityId)
    {
        $_SESSION['orgchart-move-ocid'] = $ocid;
        $_SESSION['orgchart-move-entity'] = $entityId;

        die;
    }


    public function Movecancel()
    {
        unset($_SESSION['orgchart-move-ocid']);
        unset($_SESSION['orgchart-move-entity']);

        die;
    }

    public function MoveToThis($ocid, $entityId, $typemove)
    {
        if($ocid != $_SESSION['orgchart-move-ocid']){
            return false;
        }
        $orgUtil = new bab_OrgChartUtil($ocid);
        $orgUtil->moveEntity($_SESSION['orgchart-move-entity'], $entityId, BAB_OC_MOVE_ENTITY_AND_CHILDREN, $typemove);


        require_once $GLOBALS['babInstallPath'].'utilit/grpincl.php';
        require_once $GLOBALS['babInstallPath'].'utilit/groupsincl.php';
        $entityUtil1 = $orgUtil->getEntity($_SESSION['orgchart-move-entity']);
        $entityUtil2 = $orgUtil->getEntity($entityId);

        if($entityUtil1['id_group'] > 3 && $entityUtil2['id_group'] > 3){
            if($typemove == BAB_OC_MOVE_TYPE_AS_CHILD){
                bab_moveGroup($entityUtil1['id_group'], $entityUtil2['id_group'], 2, $entityUtil1['name']);
            } else {
                $parentsGroup = bab_Groups::getAncestors($entityUtil2['id_group']);
                $parentsGroup = array_keys($parentsGroup);
                $parentsGroup = array_pop($parentsGroup);
                bab_moveGroup($entityUtil1['id_group'], $parentsGroup, 2, $entityUtil1['name']);
            }
        }

        unset($_SESSION['orgchart-move-ocid']);
        unset($_SESSION['orgchart-move-entity']);

        die;
    }



    public function Permute($ocid, $entityId)
    {
        $_SESSION['orgchart-permut-ocid'] = $ocid;
        $_SESSION['orgchart-permut-entity'] = $entityId;

        die;
    }


    public function PermuteStop()
    {
        unset($_SESSION['orgchart-permut-ocid']);
        unset($_SESSION['orgchart-permut-entity']);

        die;
    }



    public function PermuteThis($ocid, $entityId)
    {
        if (!orgchart_isManager($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        if($ocid != $_SESSION['orgchart-permut-ocid']){
            return false;
        }
        $orgUtil = new bab_OrgChartUtil($ocid);
        $orgUtil->swapEntity($_SESSION['orgchart-permut-entity'], $entityId);

        unset($_SESSION['orgchart-permut-ocid']);
        unset($_SESSION['orgchart-permut-entity']);

        return true;
    }

    private function getListsPage()
    {
        $W = bab_Widgets();
        $page = $W->BabPage();
        $page->addStyleSheet('addons/orgchart/orgchart.css');
        $page->addStyleSheet('addons/orgchart/direntry/ficheannuaire.css');

        return $page;
    }

    private function itemMenuAdmin($page)
    {
        $page->addItemMenu(
            'orgchart',
            orgchart_translate('Orgchart view'),
            $this->proxy()->displayAdminOrgchart(bab_rp('ocid'), 'orgchart')->url()
        );
        $page->addItemMenu(
            'tree',
            orgchart_translate('Tree view'),
            $this->proxy()->displayAdminOrgchart(bab_rp('ocid'), 'tree')->url()
        );

        return $page;
    }

    private function itemMenuView($page)
    {
        $page->addItemMenu(
            'orgchart',
            orgchart_translate('Orgchart view'),
            $this->proxy()->displayViewOrgchart(bab_rp('ocid'), 'orgchart')->url()
        );
        $page->addItemMenu(
            'tree',
            orgchart_translate('Tree view'),
            $this->proxy()->displayViewOrgchart(bab_rp('ocid'), 'tree')->url()
        );

        return $page;
    }


    /**
     * @throws Exception
     * @return Widget_BabPage
     */
    public function displayViewOrgchart($ocid = null, $view = '', $start = 0)
    {
        bab_functionality::includefile('Icons');

        if (!orgchart_isViewer($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $this->admin = false;

        if ($view == '') {
            $view = 'orgchart';
            if (isset($_SESSION['orgchart_view'])) {
                $view = $_SESSION['orgchart_view'];
            }
        }
        $_SESSION['orgchart_view'] = $view;

        $W = bab_Widgets();
        $page = $this->getListsPage();
        $this->itemMenuView($page);
        $page->setCurrentItemMenu($view);

        if ($view === 'orgchart') {
            $orgchartView = $W->OrgchartView('orgChart_' . $ocid);
            $orgchartView->setSaveDefaultStateAction(
                $this->proxy()->saveDefaultState($ocid)
            );

            $orgchartView->t_orgChartId = $ocid;
        } elseif ($view === 'tree') {
            $orgchartView = $W->SimpleTreeView('simpletree_' . $ocid);
        }

        $numberOfEntities = $this->generateOrg($orgchartView, $ocid, $start);

        if ($numberOfEntities > 0) {
            $orgchartView->setReloadAction(orgchart_Controller()->Orgchart()->displayAdminOrgchart($ocid, $view, $start));
            if (bab_isAjaxRequest()) {
                return $orgchartView;
            }

            $page->addItem($orgchartView);
        }

        return $page->setEmbedded(false);
    }


    /**
     * @throws Exception
     * @return Widget_BabPage
     */
    public function displayAdminOrgchart($ocid = null, $view = '', $start = 0)
    {
        bab_functionality::includefile('Icons');

        if (!orgchart_isManager($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $this->admin = true;

        $orgUtil = new bab_OrgChartUtil($ocid);
        $orgUtil->lock();

        if ($view == '') {
            $view = 'orgchart';
            if (isset($_SESSION['orgchart_view'])) {
                $view = $_SESSION['orgchart_view'];
            }
        }
        $_SESSION['orgchart_view'] = $view;

        $W = bab_Widgets();
        $page = $this->getListsPage();
        $this->itemMenuAdmin($page);
        $page->setCurrentItemMenu($view);

        if ($view == 'orgchart') {
            $orgchartView = $W->OrgchartView('orgChart_' . $ocid);

            $path = 'orgchart/' . $orgchartView->getRealId() . '/';

            $orgchartView->setSaveDefaultStateAction(
                $this->proxy()->saveDefaultState($ocid, $path)
            );

            $orgchartView->setAdminMode();

            $orgchartView->t_orgChartId = $ocid;
        } elseif ($view == 'tree') {
            $orgchartView = $W->SimpleTreeView('simpletree_' . $ocid);
        }

        $numberOfEntities = $this->generateOrg($orgchartView, $ocid, $start);

        if ($numberOfEntities > 0) {
            $orgchartView->setReloadAction(orgchart_Controller()->Orgchart()->displayAdminOrgchart($ocid, $view, $start));
            if (bab_isAjaxRequest()) {
                return $orgchartView;
            }

            $page->addItem($orgchartView);
        } else {
            $linkCreate = $W->Frame()->addItem(
                $W->Link(
                    orgchart_translate('Create the parent entity'),
                    orgchart_Controller()->Orgchart()->editEntity($ocid, null, 0)
                )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
            )->setReloadAction(orgchart_Controller()->Orgchart()->displayAdminOrgchart($ocid, $view, $start))
            ->addClass(Func_Icons::ICON_LEFT_24);
            if (bab_isAjaxRequest()) {
                return $linkCreate;
            }
            $page->addItem($linkCreate);
        }

        return $page->setEmbedded(false);
    }


    /**
     *
     * @param Widget_OrgchartView|Widget_SimpleTreeView $orgchartView
     * @param int $ocid
     * @param int $startEntityId
     * @return int      The number of entities in the generated view
     */
    private function generateOrg($orgchartView, $ocid, $startEntityId = 0)
    {
        if ($orgchartView instanceof Widget_OrgchartView) {
            $relativeThreshold = $orgchartView->hasRelativeThreshold();
            if ($relativeThreshold) {
                global $babDB;
                $locationElements = array();

                $query = bab_OCGetPathToNodeQuery($startEntityId);
                $res = $babDB->db_query($query);

                while ($arr = $babDB->db_fetch_assoc($res)) {
                    $locationElements[$arr['iIdEntity']] = $arr['sName'];
                };

                $locationElements = array_reverse($locationElements, true);

                $verticalThreshold = $orgchartView->getVerticalThreshold();
                $orgchartView->setVerticalThreshold($verticalThreshold - count($locationElements));
            }
        }

        $numberOfEntities = $this->_addEntities($orgchartView, $ocid, $startEntityId);

        return $numberOfEntities;
    }



    private function _addEntities($orgchartView, $ocid, $startEntityId)
    {
        require_once $GLOBALS['babInstallPath'].'utilit/ocapi.php';
        global $babDB;

        $elementIdPrefix = 'ENT';
        $entityType = 'entity';

        $entities = $this->_selectEntities($ocid, $startEntityId);
        $count = 0;
        while ($entity = $babDB->db_fetch_assoc($entities)) {
            if($entity['id']){
                $entityType = 'entity';
                $count++;
                $queryType = bab_OCGetEntityTypes($entity['id']);
                if($count == 1){
                    $orgchartView->t_entityId = $entity['id'];
                }

                $entityTypes = array();
                while ($entityTy = $babDB->db_fetch_assoc($queryType)) {
                    $entityTypes[] = $entityTy;
                }

                foreach($entityTypes as $type) {
                    $entityType .= ' ' . strtr($type['name'], ' ', '_');
                }

                $element =& $this->_addEntity(
                    $orgchartView,
                    $ocid,
                    $startEntityId,
                    $entity['id'],
                    $entity['id_parent'],
                    $entityType,
                    $entity['name'],
                    $entity['id_group']
                );

                if($_SESSION['orgchart_view'] == 'tree'){
                    $element->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/category.png');
                }

                $idparent = $elementIdPrefix . $entity['id_parent'];
                if($entity['id_parent'] == 0 || $entity['id'] == $startEntityId){
                    $idparent = null;
                }
                $orgchartView->appendElement(
                    $element,
                    $idparent
                );
            }
        }
        return $count;
    }

    private function &_addEntity($orgchartView, $ocid, $startEntityId, $entityId, $entityParentId, $entityType, $entityName, $id_group)
    {
        $moveentity = '';
        if(isset($_SESSION['orgchart-move-entity']) && $_SESSION['orgchart-move-entity'] == $entityId){
            $moveentity = ' entity-move';
        }

        $elementIdPrefix = 'ENT';
        $element =& $orgchartView->createElement(
            $elementIdPrefix . $entityId,
            $entityType .$moveentity,
            bab_toHtml($entityName),
            '',
            ''
        );
        $this->_addMembers($element, $entityId, $ocid);

        /*$element->setLink(
            'javascript:'
            . "bab_updateFlbFrame('" . $GLOBALS['babUrlScript'] . "?tg=fltchart&rf=0&ocid=" . $ocid . "&oeid=" . $entityId . "&idx=detr');"
            . "bab_updateFltFrame('" . $GLOBALS['babUrlScript'] . "?tg=fltchart&rf=0&ocid=" . $ocid . "&oeid=" . $entityId . "&idx=listr');
            changestyle('ENT" . $entityId . "','BabLoginMenuBackground','BabTopicsButtonBackground');"
        );*/

        $this->_addActions($element, $entityId, $entityParentId, $ocid, $startEntityId, $id_group);

        return $element;
    }

    private function _getDirEntry($id = false) {
        include_once $GLOBALS['babInstallPath']."utilit/dirincl.php";
        global $babDB;

        $res = $babDB->db_query("
            SELECT
                e.id,
                e.sn,
                e.givenname,
                LENGTH(e.photo_data) photo_data
            FROM
                ".BAB_DBDIR_ENTRIES_TBL." e
            WHERE
                e.id = ".$babDB->quote($id)."
        ");



        while( $arr = $babDB->db_fetch_assoc($res)) {
            $jpegphoto = null;
            if ($arr['photo_data'] > 0) {
                $photo = new bab_dirEntryPhoto($arr['id']);
                $jpegphoto['photo'] = $photo;
            }
            return array(
                'givenname' => array('value' => $arr['givenname']),
                'sn' => array('value' => $arr['sn']),
                'jpegphoto' => $jpegphoto
            );
        }
    }

    private function _addMembers(&$element, $entityId, $ocid)
    {
        require_once $GLOBALS['babInstallPath'].'utilit/ocapi.php';
        global $babDB;

        static $infoOC = null;
        if($infoOC === null){
            $infoOC = bab_OCGetOrgChart($ocid);
        }

        $members = bab_OCselectEntityCollaborators($entityId, false, false, true);
        while ($member = $babDB->db_fetch_assoc($members)) {
            if($member['role_type'] == BAB_OC_ROLE_TEMPORARY_EMPLOYEE){
                continue;
            }
            $dirEntry = $this->_getDirEntry($member['id_dir_entry']);

            if (isset($dirEntry['givenname']) && isset($dirEntry['sn'])) {
                $memberName = bab_toHtml(bab_composeUserName($dirEntry['givenname']['value'], $dirEntry['sn']['value']));
                if ($member['user_disabled']) {
                    $memberName = '<span title="'.orgchart_translate('Disabled user').'" style="color:red">&times;</span> '.$memberName;
                }
                if ($member['role_type'] == 1) {
                    if (isset($dirEntry['jpegphoto'])) {
                        $photo = $dirEntry['jpegphoto']['photo'];
                        $dirEntry['jpegphoto']['value'] = $photo->getUrl();
                        if(!empty($dirEntry['jpegphoto']['value'])){
                            /* @var $T Func_Thumbnailer */
                            $T = @bab_functionality::get('Thumbnailer');

                            if ($T) {
                                // The thumbnailer functionality is available.

                                if(isset($dirEntry['jpegphoto']['photo'])){
                                    $T->setSourceBinary($dirEntry['jpegphoto']['photo']->getData(), $dirEntry['jpegphoto']['photo']->lastUpdate());
                                    $element->setIcon($T->getThumbnail(150, 150));
                                }
                            }else{
                                $element->setIcon($dirEntry['jpegphoto']['value'] . '&width=150&height=150');
                            }
                        }
                    }
                    $memberUrl = orgchart_Controller()->Orgchart()->displayDirEntry($ocid, $infoOC['id_directory'], $member['id_dir_entry'], $this->admin)->url();
                    $element->setInfo($memberName);
                    $element->setLink($memberUrl);
                }

                $memberUrl = orgchart_Controller()->Orgchart()->displayDirEntry($ocid, $infoOC['id_directory'], $member['id_dir_entry'], $this->admin)->url();

                $element->addMember($memberName, $member['role_name'], $memberUrl);
            }

        }
    }

    public function changeMainRole($ocid, $idmember, $role = null)
    {
        if (!orgchart_isManager($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $orgUtil = new bab_OrgChartUtil($ocid);
        $orgUtil->updateUserPrimaryRole($idmember, $role);
        die(true);
    }

    public function displayDirEntry($ocid, $dir, $entry, $admin = false)
    {
        if (!orgchart_isViewer($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $W = bab_Widgets();
        $page = $W->BabPage();

        if(bab_isAccessValid(BAB_DBDIRVIEW_GROUPS_TBL, $dir)){
            $args = array();
            $args['directoryid'] = $dir;
            $args['userid'] = $entry;
            $args['mainRoleTxt'] = orgchart_translate('Main role');

            $args['SelectMainRole'] = '';

            $select = $W->Select()->setName('role')
                ->setAjaxAction(
                    orgchart_Controller()->Orgchart()->changeMainRole($ocid, $entry),
                    array(),
                    'change'
                );

            $orgUtil = new bab_OrgChartUtil($ocid);
            $roles = $orgUtil->getUserRoles($entry);
            $content = array();

            $hasNoPrimaryRole = true;
            foreach($roles as $role){
                $select->addOption($role['id_ru'], $role['name'] . ': ' . $role['r_name']);
                if($role['isprimary'] == 'Y'){
                    $hasNoPrimaryRole = false;
                    $select->setValue($role['id_ru']);
                    $content[] = array(
                        'entity' => '1_._'. $role['name'],
                        'role' => $role['r_name']
                    );
                }else{
                    $content[] = array(
                        'entity' => '0_._'. $role['name'],
                        'role' => $role['r_name']
                    );
                }

            }

            if($hasNoPrimaryRole && $role){
                $orgUtil->updateUserPrimaryRole($entry, $role['id_ru']);
                $select->setValue($role['id_ru']);
            }

            $header = array(
                'entity' => orgchart_translate('Entiy'),
                'role' => orgchart_translate('Role')
            );

            $canvas = $W->HtmlCanvas();

            $table = new orgchart_RoleArrayView($header, $content, 'role');
            $table->addClass(Func_Icons::ICON_LEFT_16);
            $args['Roles'] = $table->display($canvas);

            if($admin && orgchart_isManager($ocid)){
                $args['SelectMainRole'] = $select->display($canvas);
            }

            $addon = bab_getAddonInfosInstance('orgchart');
            echo bab_printOvml( implode('', file($addon->getTemplatePath().'direntry.ovml')), $args );
            die;
        }

        if(bab_isAjaxRequest()) {
            return $W->label(orgchart_translate('Access denied'));
        }

        return $page->addItem($W->label(orgchart_translate('Access denied')));
    }

    private function _addActions(&$element, $entityId, $entityParentId, $ocid, $startEntityId, $id_group)
    {
        if ($entityId != $startEntityId) {
            if($entityParentId !== null){
                $element->addAction(
                    'show_from_here',
                    orgchart_translate("Show orgchart from this entity"),
                    $GLOBALS['babSkinPath'] . 'images/Puces/bottom.png',
                    bab_toHtml(orgchart_Controller()->Orgchart()->displayAdminOrgchart($ocid, bab_rp('view'), $entityId)->url()),
                    ''
                );
            }
        } else if ($entityParentId != 0) {
            $element->addAction(
                'show_from_parent',
                orgchart_translate("Show orgchart from parent entity"),
                $GLOBALS['babSkinPath'] . 'images/Puces/parent.gif',
                bab_toHtml(orgchart_Controller()->Orgchart()->displayAdminOrgchart($ocid, bab_rp('view'), $entityParentId)->url()),
                ''
            );
        }

        if($this->admin && orgchart_isManager($ocid)){
            $element->addAction(
                'edit_entity',
                orgchart_translate("Edit this entity"),
                $GLOBALS['babSkinPath'] . 'images/Puces/edit.png',
                bab_toHtml(orgchart_Controller()->Orgchart()->editEntity($ocid, $entityId)->url()),
                '',
                array('this'),
                'widget-link widget-popup-dialog-and-reload'
            );

            if (!$id_group) {
                $element->addAction(
                    'create_group',
                    orgchart_translate("Create associated group"),
                    $GLOBALS['babSkinPath'] . 'images/Puces/folder_group.gif',
                    bab_toHtml(orgchart_Controller()->Orgchart()->confirmCreateGroup($ocid, $entityId)->url()),
                    '',
                    array('this'),
                    'widget-link widget-popup-dialog-and-reload'
                );
            }

            $element->addAction(
                'add_child_entity',
                orgchart_translate("Add a child entity"),
                $GLOBALS['babSkinPath'] . 'images/Puces/edit_add.png',
                bab_toHtml(orgchart_Controller()->Orgchart()->editEntity($ocid, null, $entityId)->url()),
                '',
                array('this'),
                'widget-link widget-popup-dialog-and-reload'
            );
        }

        if(bab_rp('view') != 'tree'){
            $element->addAction(
                'toggle_members',
                orgchart_translate("Show/Hide entity members"),
                $GLOBALS['babSkinPath'] . 'images/Puces/members.png',
                '',
                'toggleMembers'
            );
        }

        if($this->admin && orgchart_isManager($ocid)){
            $element->addAction(
                'edit',
                orgchart_translate("Manage Roles and Members"),
                $GLOBALS['babSkinPath'] . 'images/Puces/head.gif',
                bab_toHtml(orgchart_Controller()->Orgchart()->ManageMembers($ocid, $entityId)->url()),
                '',
                array('this'),
                'widget-link widget-popup-dialog-and-reload'
            );

            $element->addAction(
                'edit',
                orgchart_translate("Approbators"),
                $GLOBALS['babSkinPath'] . 'images/Puces/confirm.gif',
                bab_toHtml(orgchart_Controller()->Orgchart()->EditApprobators($ocid, $entityId)->url()),
                '',
                array('this'),
                'widget-link widget-popup-dialog-and-reload'
            );

            if($entityParentId !== null){ // The root entity cannot be removed
                if(isset($_SESSION['orgchart-permut-ocid']) && isset($_SESSION['orgchart-permut-entity'])){
                    $element->addAction(
                        'replaceWithThis',
                        orgchart_translate("Permute this entity with head"),
                        $GLOBALS['babSkinPath'] . 'images/Puces/action_success.gif',
                        bab_toHtml(orgchart_Controller()->Orgchart()->PermuteThis($ocid, $entityId)->url()),
                        '',
                        array('this'),
                        'widget-link widget-link-ajax-action'
                    );
                }else{
                    if((!isset($_SESSION['orgchart-move-ocid']) || !isset($_SESSION['orgchart-move-entity']))){
                        $element->addAction(
                            'move',
                            orgchart_translate("Move this entity"),
                            $GLOBALS['babSkinPath'] . 'images/Puces/bottom.png',
                            bab_toHtml(orgchart_Controller()->Orgchart()->Move($ocid, $entityId)->url()),
                            '',
                            array('this'),
                            'widget-link widget-link-ajax-action'
                        );
                    }else{
                        if($_SESSION['orgchart-move-entity'] == $entityId){
                            $element->addAction(
                                'cancelmove',
                                orgchart_translate("Cancel move"),
                                $GLOBALS['babSkinPath'] . 'images/Puces/action_fail.gif',
                                bab_toHtml(orgchart_Controller()->Orgchart()->Movecancel()->url()),
                                '',
                                array('this'),
                                'widget-link widget-link-ajax-action'
                            );
                        }else{
                            $childrens = bab_OCGetChildren($_SESSION['orgchart-move-entity']);
                            if(!isset($childrens[$entityId])){
                                $element->addAction(
                                    'moveLeft',
                                    orgchart_translate("Move selected entity before this one"),
                                    $GLOBALS['babSkinPath'] . 'images/Puces/go-previous.png',
                                    bab_toHtml(orgchart_Controller()->Orgchart()->MoveToThis($ocid, $entityId, BAB_OC_MOVE_TYPE_AS_PREVIOUS_SIBLING)->url()),
                                    '',
                                    array('this'),
                                    'widget-link widget-link-ajax-action'
                                );
                                $element->addAction(
                                    'moveDown',
                                    orgchart_translate("Move selected entity to the last child"),
                                    $GLOBALS['babSkinPath'] . 'images/Puces/go-bottom.png',
                                    bab_toHtml(orgchart_Controller()->Orgchart()->MoveToThis($ocid, $entityId, BAB_OC_MOVE_TYPE_AS_CHILD)->url()),
                                    '',
                                    array('this'),
                                    'widget-link widget-link-ajax-action'
                                );
                                $element->addAction(
                                    'moveRight',
                                    orgchart_translate("Move selected entity after this one"),
                                    $GLOBALS['babSkinPath'] . 'images/Puces/go-next.png',
                                    bab_toHtml(orgchart_Controller()->Orgchart()->MoveToThis($ocid, $entityId, BAB_OC_MOVE_TYPE_AS_NEXT_SIBLING)->url()),
                                    '',
                                    array('this'),
                                    'widget-link widget-link-ajax-action'
                                );
                                }
                        }
                    }
                }
                $element->addAction(
                    'delete',
                    orgchart_translate("Delete"),
                    $GLOBALS['babSkinPath'] . 'images/Puces/del.gif',
                    bab_toHtml(orgchart_Controller()->Orgchart()->deleteConfirmEntity($ocid, $entityId)->url()),
                    '',
                    array('this'),
                    'widget-link widget-popup-dialog-and-reload'
                );
            }else{
                if(!isset($_SESSION['orgchart-permut-ocid']) || !isset($_SESSION['orgchart-permut-entity'])){
                    $element->addAction(
                        'replace',
                        orgchart_translate("Permute this entity"),
                        $GLOBALS['babSkinPath'] . 'images/Puces/update.gif',
                        bab_toHtml(orgchart_Controller()->Orgchart()->Permute($ocid, $entityId)->url()),
                        '',
                        array('this'),
                        'widget-link widget-link-ajax-action'
                    );
                }else{
                    $element->addAction(
                        'stopreplace',
                        orgchart_translate("Stop permutation"),
                        $GLOBALS['babSkinPath'] . 'images/Puces/action_fail.gif',
                        bab_toHtml(orgchart_Controller()->Orgchart()->PermuteStop()->url()),
                        '',
                        array('this'),
                        'widget-link widget-link-ajax-action'
                    );
                }
            }
        }
    }

    private function _selectEntities($ocid, $startEntityId)
    {
        global $babDB;

        $where = array('trees.id_user = ' . $babDB->quote($ocid));

        if ($startEntityId != 0) {
            $sql = 'SELECT trees.id, trees.lf, trees.lr ';
            $sql .= ' FROM ' . BAB_OC_TREES_TBL . ' AS trees';
            $sql .= ' LEFT JOIN ' . BAB_OC_ENTITIES_TBL . ' AS entities on entities.id_node=trees.id';
            $sql .= ' WHERE trees.id_user = ' . $babDB->quote($ocid);
            $sql .= ' AND entities.id = ' . $babDB->quote($startEntityId);
            $trees = $babDB->db_query($sql);
            $tree = $babDB->db_fetch_assoc($trees);

            $where[] = '(trees.id = ' . $babDB->quote($tree['id']) .
                        ' OR (trees.lf > ' . $babDB->quote($tree['lf']) .
                            ' AND trees.lr < '  . $babDB->quote($tree['lr']) . '))';
        }


        $sql = 'SELECT entities.*, entities2.id as id_parent ';
        $sql .= ' FROM ' . BAB_OC_TREES_TBL . ' AS trees';
        $sql .= ' LEFT JOIN ' . BAB_OC_ENTITIES_TBL . ' AS entities ON entities.id_node = trees.id';
        $sql .= ' LEFT JOIN ' . BAB_OC_ENTITIES_TBL . ' AS entities2 ON entities2.id_node = trees.id_parent';

        $sql .= ' WHERE ' . implode(' AND ', $where);
        $sql .= ' ORDER BY trees.lf ASC';

        $entities = $babDB->db_query($sql);

        return $entities;
    }




    /**
     *
     * @param int $ocid
     * @param string $path
     * @param array $values
     * @throws bab_AccessException
     * @return boolean
     */
    public function saveDefaultState($ocid, $path = '', $values = null)
    {
        if (!orgchart_isManager($ocid)) {
            throw new bab_AccessException(orgchart_translate('Access denied'));
        }

        $W = bab_Widgets();

        $addon = 'widgets';

        if (isset($values) && is_array($values)) {
            foreach ($values as $key => $value) {
                $W->setDefaultConfiguration($path . $key, $value, $addon);
            }
        }

        $this->addMessage(orgchart_translate('The default view has been saved.'));

        return true;
    }



    public function cancel()
    {
        return true;
    }
}

