<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';
require_once $GLOBALS['babInstallPath']."utilit/dateTime.php";


bab_Widgets()->includePhpClass('widget_Form');
bab_Widgets()->includePhpClass('Widget_BabTableArrayView');


class orgchart_TableArrayView extends Widget_BabTableArrayView
{
    protected $pageLength = 20;

    public function computeCellContent($content, $key, $id)
    {
        $W = bab_Widgets();
        static $controller = null;
        if($controller == null){
            $controller = orgchart_Controller()->Orgchart();
        }

        if($key == '_actions')
        {
            $layout = $W->HBoxItems()->setHorizontalSpacing(1, 'em');
            $orgUtil = new bab_OrgChartUtil($content[$key]);
            $lock = $orgUtil->getLockInfo();

            if($orgUtil->isAccessValid())
            {
                $layout->addItem(
                    $W->Link(
                        $W->Icon(
                            orgchart_translate('View'),
                            Func_Icons::ACTIONS_VIEW_LIST_TREE
                        ),
                        $controller->displayViewOrgchart($content[$key])
                    )->addAttribute('target', '_blank')
                );
            }

            if($orgUtil->bHaveUpdateRight)
            {
                if($lock['iIdUser'] && $lock['iIdUser'] != bab_getUserId())
                {
                    $layout->addItem(
                        $W->Label(orgchart_translate('Locked by ') . $lock['sFirstName'] . ' ' . $lock['sLastName'])
                    );
                }else{
                    $layout->addItem(
                        $W->Link(
                            $W->Icon(
                                orgchart_translate('Update'),
                                Func_Icons::ACTIONS_DOCUMENT_EDIT
                            ),
                            $controller->displayAdminOrgchart($content[$key])
                        )->addAttribute('target', '_blank')
                    );
                }
            }

            if($lock['iIdUser'] && ($lock['iIdUser'] == bab_getUserId() || bab_isUserAdministrator()))
            {
                if($lock['iIdUser'] != bab_getUserId()){
                    $layout->addItem(
                        $W->Link(
                            $W->Icon(
                                orgchart_translate('Force unlock'),
                                Func_Icons::STATUS_DIALOG_WARNING
                            ),
                            $controller->unlock($content[$key])
                        )->setConfirmationMessage(sprintf(orgchart_translate('This will unlock this orgchart all unsaved update done by the user %s will be lost.'), $lock['sFirstName'] . ' ' . $lock['sLastName']))
                        ->setTitle(orgchart_translate('This orgchart is locked by an other user but can be unlock by you because your are an administrator.'))
                    );
                }else{
                    $layout->addItem(
                        $W->Link(
                            $W->Icon(
                                orgchart_translate('Unlock'),
                                Func_Icons::APPS_PREFERENCES_AUTHENTICATION
                            ),
                            $controller->unlock($content[$key])
                        )
                    );
                }
            }

            if($orgUtil->bHaveUpdateRight && (!$lock['iIdUser']  || $lock['iIdUser'] == bab_getUserId()) && bab_isUserAdministrator()){
                $root = $orgUtil->getRoot();
                if($root) {
                    $rootInfo = $orgUtil->getEntity($root['id_entity']);

                    require_once $GLOBALS['babInstallPath'].'utilit/grpincl.php';
                    require_once $GLOBALS['babInstallPath'].'utilit/groupsincl.php';

                    $group = orgchart_getRootGroup();
                    

                    if($rootInfo['id_group'] !== null && bab_isGroup($rootInfo['id_group'])){
                        $rootGroupInfo = bab_Groups::get($rootInfo['id_group']);
                        if(isset($rootGroupInfo['id_parent']) && $rootGroupInfo['id_parent']){
                            $ocGroupInfo = bab_Groups::get($rootGroupInfo['id_parent']);
                        }else{
                            $ocGroupInfo['id_parent'] = -1;
                        }
                    }else{
                        $rootInfo['id_group'] = 0;
                    }

                    if($rootInfo['id_group'] == 0 || $ocGroupInfo['id_parent'] != $group){
                        $layout->addItem(
                            $W->Link(
                                $W->Icon(
                                    orgchart_translate('Reconstruct group'),
                                    Func_Icons::STATUS_DIALOG_WARNING
                                ),
                                $controller->displayReconstruct($content[$key])
                            )->setTitle(orgchart_translate('Only ogchart without group association can be reconstruct.'))
                        );
                    }
                } else {
                    
                    
                    $layout->addItem(
                    $W->Link(
                        $W->Icon(
                            orgchart_translate('Create entities from groups'),
                            Func_Icons::STATUS_DIALOG_WARNING
                            ),
                        $controller->reconstructFromGroup($content[$key])
                        )->setConfirmationMessage(orgchart_translate('Entities will be created using groups, continue?'))
                    );
                }
            }
            return $layout;
        }

        return $W->Label($content[$key]);
    }
}

class orgchart_RoleArrayView extends Widget_TableArrayView
{
    protected $pageLength = 20;

    public function computeCellContent($content, $key, $id)
    {
        $W = bab_Widgets();

        if($key == 'entity')
        {
            $values = explode('_._', $content[$key]);
            if($values[0]){
                return $W->Icon($values[1], Func_Icons::PLACES_FOLDER_BOOKMARKS);
            }
            return $W->Label($values[1]);
        }

        return $W->Label($content[$key]);
    }
}





/**
 * Formulaire
 *
 */
class orgchart_EntityEditor extends Widget_Form {

    protected $currentEntity;
    protected $parentEntity;
    protected $orgChart;
    protected $editEntity = false;


    public function __construct($ocid, $currentEntity = null, $parentEntity = null)
    {
        $W = bab_Widgets();

        $this->currentEntity = $currentEntity;
        $this->parentEntity = $parentEntity;
        $this->orgChart = $ocid;

        parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));

        $this->setHiddenValue('tg', bab_rp('tg'));
        $this->setName('entity');
        $this->colon();
        $this->addFields();
        $this->addButtons();

        $this->addClass('widget-bordered');
        $this->addClass('widget-centered');
        $this->addClass('BabLoginMenuBackground');


        $this->setHiddenValue('entity[id]', (string) $this->currentEntity);
        $this->setHiddenValue('entity[parent]', (string) $this->parentEntity);
        $this->setHiddenValue('entity[orgchart]', (string) $this->orgChart);

        $entity = bab_OCGetEntity($this->currentEntity);

        if($entity){
            $this->editEntity = true;
            $this->setValues(
                array(
                    'entity' => array(
                        'name' => $entity['name'],
                        'description' => $entity['description']
                    )
                )
            );
        }else{
            $this->setValues(array('entity' => bab_pp('entity')));
        }


    }

    protected function addFields()
    {
        $W = bab_Widgets();

        $name = $W->LabelledWidget(
            orgchart_translate('Name'),
            $W->LineEdit()->setMandatory(true, orgchart_translate('The name is mandatory.')),
            'name'
        );

        $desc = $W->LabelledWidget(
            orgchart_translate('Description'),
            $W->LineEdit(),
            'description'
        );

        $this->addItem($name);
        $this->addItem($desc);
    }


    protected function addButtons()
    {
        $W = bab_Widgets();

        $ctrl = orgchart_Controller()->Orgchart();
        $view = bab_rp('view', 'orgchart');

        $save = $W->SubmitButton()->setLabel(orgchart_translate('Save'))
            ->setAction($ctrl->saveEntity());

        $this->addItem(
            $save
        );
    }
}





/**
 * Formulaire
 *
 */
class orgchart_RoleEditor extends Widget_Form {

    protected $ocid;
    protected $entityId;
    protected $roleId;
    protected $editEntity = false;


    public function __construct($ocid, $entityId, $roleId = null)
    {
        $W = bab_Widgets();

        $this->ocid = $ocid;
        $this->entityId = $entityId;
        $this->roleId = $roleId;

        parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));

        $this->setHiddenValue('tg', bab_rp('tg'));
        $this->setName('role');
        $this->colon();
        $this->addFields();
        $this->addButtons();

        $this->addClass('widget-bordered');
        $this->addClass('widget-centered');
        $this->addClass('BabLoginMenuBackground');


        $this->setHiddenValue('role[id]', $this->roleId);
        $this->setHiddenValue('role[ocid]', $this->ocid);
        $this->setHiddenValue('role[entity]', $this->entityId);

        $orgUtil = new bab_OrgChartUtil($ocid);

        if($roleId){
            $role = $orgUtil->getRoleById($roleId);
            $this->editRole = true;
            $this->setValues(
                array(
                    'role' => array(
                        'name' => $role['name'],
                        'description' => $role['description']
                    )
                )
            );
        }else{
            $this->setValues(array('role' => bab_pp('role')));
        }


    }

    protected function addFields()
    {
        $W = bab_Widgets();

        $name = $W->LabelledWidget(
            orgchart_translate('Name'),
            $W->LineEdit()->setMandatory(true, orgchart_translate('The name is mandatory.')),
            'name'
        );

        $desc = $W->LabelledWidget(
            orgchart_translate('Description'),
            $W->LineEdit(),
            'description'
        );

        $this->addItem($name);
        $this->addItem($desc);
    }


    protected function addButtons()
    {
        $W = bab_Widgets();

        $ctrl = orgchart_Controller()->Orgchart();
        $ctrl = orgchart_Controller()->Orgchart();

        $save = $W->SubmitButton()->setLabel(orgchart_translate('Save'))
            ->setAjaxAction($ctrl->saveRole());

        $this->addItem(
            $save
        );
    }
}



/**
* Confirm group creation
*
*/
class orgchart_CreateGroupEditor extends Widget_Form 
{

    protected $entity;
    protected $orgChart;


    public function __construct($ocid, $entity)
    {
        $W = bab_Widgets();

        $this->entity = $entity;
        $this->orgChart = $ocid;

        parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));


        $entityInfo = bab_OCGetEntity($this->entity);

        $this->addItem($W->Title(orgchart_translate('Entity') . ': '.$entityInfo['name'], 3));

        $this->setHiddenValue('tg', bab_rp('tg'));
        $this->setName('entity');
        $this->colon();
        $this->addItem($W->Label(orgchart_translate('Do you really want to create an associated group?')));
        $this->addButtons();

        $this->addClass('widget-bordered');
        $this->addClass('widget-centered');
        $this->addClass('BabLoginMenuBackground');


        $this->setHiddenValue('entity[id]', $this->entity);
        $this->setHiddenValue('entity[orgchart]', $this->orgChart);

        $this->setValues(array('entity' => bab_pp('entity')));

    }


    protected function addButtons()
    {
        $W = bab_Widgets();

        $ctrl = orgchart_Controller()->Orgchart();
        $view = bab_rp('view', 'orgchart');
        $back = $ctrl->displayAdminOrgchart($this->orgChart, $view, 0);

        $save = $W->SubmitButton()->setLabel(orgchart_translate('Create group'))
        ->setAction($ctrl->createGroup())
        ->setSuccessAction($back)
        ->setFailedAction($back);




        $cancel = $W->SubmitButton()->setLabel(orgchart_translate('Cancel'))
        ->setAction($ctrl->cancel())
        ->setSuccessAction($back)
        ->setFailedAction($back);

        $this->addItem(
            $W->FlowItems($save, $cancel)->setSpacing(1,'em')
        );
    }
}





/**
 * Formulaire
 *
 */
class orgchart_EntityRemover extends Widget_Form {

    protected $entity;
    protected $orgChart;
    protected $editEntity = false;


    public function __construct($ocid, $entity)
    {
        $W = bab_Widgets();

        $this->entity = $entity;
        $this->orgChart = $ocid;

        parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));


        $entityInfo = bab_OCGetEntity($this->entity);

        $this->addItem($W->Title(orgchart_translate('Entity') . ': '.$entityInfo['name'], 3));

        $this->setHiddenValue('tg', bab_rp('tg'));
        $this->setName('entity');
        $this->colon();
        $this->addFields();
        $this->addButtons();

        $this->addClass('widget-bordered');
        $this->addClass('widget-centered');
        $this->addClass('BabLoginMenuBackground');


        $this->setHiddenValue('entity[id]', $this->entity);
        $this->setHiddenValue('entity[orgchart]', $this->orgChart);

        $this->setValues(array('entity' => bab_pp('entity')));

    }

    protected function addFields()
    {
        $W = bab_Widgets();

        $radioSet = $W->RadioSet()->setValue(1)->setName('method')->setMandatory(true, orgchart_translate('The Method is mandatory.'));

        $method1 = $W->LabelledWidget(
            orgchart_translate('Remove this entity and its children'),
            $tmpRadio = $W->Radio(null, $radioSet)->setRadioSet($radioSet)->setCheckedValue(1),
            'method',
            orgchart_translate('This will also remove associated groups for this entity and its children. All access rights link to those groups will be lost.')
        );
        $radioSet->attachRadio($tmpRadio);

        $method2 = $W->LabelledWidget(
            orgchart_translate('Remove this entity and link its children to the parent entity'),
            $tmpRadio = $W->Radio(null, $radioSet)->setCheckedValue(2),
            'method',
            orgchart_translate('This will only remove the associated groups for this entity. Groups associated to children entities will be attach to the parent entiy group. Children groups may lost some rights.')
        );
        $radioSet->attachRadio($tmpRadio);

        $this->addItem($radioSet);
        $this->addItem($method1);
        //$this->addItem($method2);
    }


    protected function addButtons()
    {
        $W = bab_Widgets();

        $ctrl = orgchart_Controller()->Orgchart();
        $view = bab_rp('view', 'orgchart');

        $save = $W->SubmitButton()->setLabel(orgchart_translate('Remove'))
            ->setAction($ctrl->deleteEntity())
            ->setSuccessAction($ctrl->displayAdminOrgchart($this->orgChart, $view, 0))
            ->setFailedAction($ctrl->editEntity($this->orgChart, $this->entity, $view));




        $cancel = $W->SubmitButton()->setLabel(orgchart_translate('Cancel'))
            ->setAction($ctrl->cancel())
            ->setSuccessAction($ctrl->displayAdminOrgchart($this->orgChart, $view, 0))
            ->setFailedAction($ctrl->editEntity($this->orgChart, $this->entity, $view));

        $this->addItem(
            $W->FlowItems($save, $cancel)->setSpacing(1,'em')
        );
    }
}




/**
 * Formulaire
 *
 */
class orgchart_RoleRemover extends Widget_Form {

    protected $role;
    protected $ocid;


    public function __construct($ocid, $role)
    {
        $W = bab_Widgets();

        $this->role = $role;
        $this->ocid = $ocid;

        parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));


        $roleinfo = bab_OCGetRole($role);;


        $this->addItem($W->Title(orgchart_translate('Role') . ': '.$roleinfo['name'], 3));

        $this->setHiddenValue('tg', bab_rp('tg'));
        $this->setName('role');
        $this->colon();
        $this->addFields();
        $this->addButtons();

        $this->addClass('widget-bordered');
        $this->addClass('widget-centered');
        $this->addClass('BabLoginMenuBackground');


        $this->setHiddenValue('role[id]', $this->role);
        $this->setHiddenValue('role[ocid]', $this->ocid);

        $this->setValues(array('role' => bab_pp('role')));

    }

    protected function addFields()
    {
        $W = bab_Widgets();

        $radioSet = $W->RadioSet()->setValue(1)->setName('method')->setMandatory(true, orgchart_translate('The Method is mandatory.'));

        $method1 = $W->LabelledWidget(
            orgchart_translate('Remove this role'),
            $tmpRadio = $W->Radio(null, $radioSet)->setRadioSet($radioSet)->setCheckedValue(1),
            'method',
            orgchart_translate('This will remove all user attach to this role.') . ' ' .
            orgchart_translate('If this role was the primary for a user, the primary role of this user will be associated to one of this other role.') . ' ' .
            orgchart_translate('This will also detach user from the entity\'s group if the user is not attach to an other role.')
        );
        $radioSet->attachRadio($tmpRadio);

        $this->addItem($radioSet);
        $this->addItem($method1);
    }


    protected function addButtons()
    {
        $W = bab_Widgets();

        $ctrl = orgchart_Controller()->Orgchart();
        $view = bab_rp('view', 'orgchart');

        $save = $W->SubmitButton()->setLabel(orgchart_translate('Remove'))
            ->setAjaxAction($ctrl->removeRole());

        $cancel = $W->SubmitButton()->setLabel(orgchart_translate('Cancel'))
            ->setAjaxAction($ctrl->cancel());

        $this->addItem(
            $W->FlowItems($save, $cancel)->setSpacing(1,'em')
        );
    }
}




bab_Widgets()->includePhpClass('Widget_BabTableArrayView');
class orgchart_userRoleArrayView extends Widget_TableArrayView
{
    protected $ocid;

    public function __construct($header, $content, $name, $columns = null, $id = null, $ocid, $entity)
    {
        $this->ocid = $ocid;
        $this->entity = $entity;
        return parent::__construct($header, $content, $name, $columns, $id);
    }


    public function computeCellContent($content, $key, $id)
    {
        $W = bab_Widgets();

        if($key == '_rm')
        {
            return $W->Link(
                '',
                orgchart_Controller()->Orgchart()->removeMember($content[$key], $this->ocid)
            )->addCLass('icon',Func_Icons::ACTIONS_EDIT_DELETE)
            ->setConfirmationMessage(orgchart_translate('This will also remove the user from the group, proceed?'))
            ->setAjaxAction(orgchart_Controller()->Orgchart()->removeMember($content[$key], $this->ocid),'orgchart-userrole-list');
        }

        if($key == '_cb')
        {
            return orgchart_Controller()->Orgchart(false)->reloadDirectoryItem($content[$key], $this->ocid, $_SESSION['selected-role-'.$this->entity]);
        }

        if($key == '_rb')
        {
            global $babDB;
            static $user = null;
            static $radioSet = null;

            if($user === null){
                $usersRes = bab_OCGetUsersByRole($_SESSION['selected-role-'.$this->entity]);
                $user = $babDB->db_fetch_assoc($usersRes);
            }

            $currentCell = $W->FlowItems();
            if($radioSet === null){
                $radioSet = $W->RadioSet()
                    ->setRadioNamePath(array('dir'))
                    ->setValue($user['id_dir']);
                $currentCell->addItem($radioSet);
            }

            $checked = false;
            if($user['id_dir'] == $content[$key]){
                $checked = true;
            }

            return 	$W->Radio(null, $radioSet)
                        //->setName('id')
                        ->setAjaxAction(orgchart_Controller()->Orgchart()->setDirectoryItem($content[$key], $this->ocid, $_SESSION['selected-role-'.$this->entity]), null, 'change')
                        ->setCheckedValue($content[$key]);
        }

        return $W->Label($content[$key]);
    }

    public function computeHeaderSection()
    {
        $columns = $this->header;
        $W = bab_Widgets();
        $this->addSection('header', null, 'widget-table-header');
        $this->setCurrentSection('header');

        $j = 0;
        foreach($columns as $k => $column)
        {
            $this->addItem($W->Label($column), 0, $j);
            $j++;
        }
    }
}






/**
 * Formulaire
 *
 */
class orgchart_RoleOrderEditor extends Widget_Form {

    protected $ocid;
    protected $entityId;
    protected $editEntity = false;


    public function __construct($ocid, $entityId)
    {
        $W = bab_Widgets();

        $this->ocid = $ocid;
        $this->entityId = $entityId;

        $orgUtil = new bab_OrgChartUtil($ocid);
        $this->roles = $orgUtil->getRoleByEntityId($entityId);

        parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));

        $this->setHiddenValue('tg', bab_rp('tg'));
        $this->setName('role');
        $this->colon();

        $this->addItem($W->Label(orgchart_translate('Drag and drop the roles to reorder them')));
        $this->addFields();
        $this->addButtons();

        $this->addClass(Func_Icons::ICON_LEFT_16);
        $this->addClass('widget-bordered');
        $this->addClass('widget-centered');
        $this->addClass('BabLoginMenuBackground');

        $this->setHiddenValue('role[ocid]', $this->ocid);
        $this->setHiddenValue('role[entity]', $this->entityId);
    }

    protected function addFields()
    {
        $W = bab_Widgets();

        $sortLayout = $W->VBoxLayout('orgchart-sort-role')->sortable();
        foreach($this->roles as $role) {
            if($role['type'] != 2){
                if($role['type'] == 1){
                    $icon = Func_Icons::OBJECTS_MANAGER;
                }elseif($role['type'] == 3){
                    $icon = Func_Icons::OBJECTS_GROUP;
                }else{
                    $icon = Func_Icons::OBJECTS_GROUP;
                }
                $sortLayout->addItem(
                    $W->FlowItems(
                        $W->Hidden()->setValue($role['ordering'])->setName($role['id']),
                        $W->Icon($role['name'], $icon)
                    )
                );
            }
        }

        $this->addItem($W->NamedContainer('order')->addItem($sortLayout));
    }


    protected function addButtons()
    {
        $W = bab_Widgets();

        $ctrl = orgchart_Controller()->Orgchart();

        $save = $W->SubmitButton()
            ->setLabel(orgchart_translate('Save order'))
            ->setAjaxAction($ctrl->saveRoleOrder());

        $this->addItem($save);
    }
}



function orgchart_userRoleFilter($entity)
{
    $W = bab_Widgets();

    $form = $W->Form()
    ->setReadOnly(true)
    ->setHiddenValue('filterform[entity]', $entity)
    ->setLayout($W->FlowItems()->setHorizontalSpacing(2,'em')->setVerticalAlign('bottom'));

    if(!isset($_SESSION['MembersFilter'.$entity])){
        $_SESSION['MembersFilter'.$entity] = '';
    }

    $form->addItem(
        $W->LabelledWidget(
            bab_translate('Search by login ID, firstname, lastname and email'),
            $W->LineEdit()->setSize(50)->setValue($_SESSION['MembersFilter'.$entity]),
            'text'
        )
    )->setName('filterform');

    $ctrl = orgchart_Controller()->Orgchart();
    $save = $W->SubmitButton()
        ->setLabel(orgchart_translate('Filter'))
        ->setAjaxAction($ctrl->saveMembersForm(), 'filtered-form');

    $form->addItem($save);

    return $form;
}
