;<?php /*

[general]
name							="orgchart"
version							="0.13.10"
addon_type						="EXTENSION"
encoding						="UTF-8"
mysql_character_set_database	="latin1,utf8"
description						="Viewing and editing organization charts"
description.fr					="Visualisation et manipulation graphique d'organigrammes"
delete							=1
ov_version						="8.4.91"
php_version						="5.1.0"
addon_access_control			="0"
author							="Cantico"
icon							="icon.png"
tags                            ="extension,default"

[addons]
widgets							="1.0.128"
LibTranslate					=">=1.12.0rc3.01"

;*/